/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package services

import (
	"testing"
	"time"

	"anongit.kde.org/sysadmin/projects-api.git/models"

	"github.com/stretchr/testify/assert"
)

type GitDAO struct {
}

func NewGitDAO() *GitDAO {
	return &GitDAO{}
}

func (d *GitDAO) Age() time.Duration {
	return 0
}

func (d *GitDAO) Get(path string) (*models.Project, error) {
	if path == "calligra/krita" {
		return &models.Project{
			Repo: "kritarepo",
		}, nil
	}
	if path == "calligra/krita-extensions/krita-analogies" {
		return &models.Project{
			Repo:   "krita-analogiesrepo",
			Active: true,
		}, nil
	}
	if path == "frameworks/solid" {
		return &models.Project{
			Repo: "solidrepo",
		}, nil
	}
	if path == "calligra" {
		return nil, nil
	}
	panic("unknown path requested " + path)
}

func (d *GitDAO) List() []string {
	return []string{"calligra", "calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}
}

func (d *GitDAO) UpdateClone() string {
	return ""
}

func TestProjectFind(t *testing.T) {
	s := NewProjectService(NewGitDAO())

	t.Run("no filter", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}, ret)
	})

	t.Run("filter by id", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{ID: "krita"})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})

	t.Run("filter by repo", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{RepoPath: "kritarepo"})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})

	t.Run("filter by active", func(t *testing.T) {
		ret, err := s.Find(&models.ProjectFilter{ActiveOnly: true})
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita-extensions/krita-analogies"}, ret)
	})
}

func TestProjectList(t *testing.T) {
	s := NewProjectService(NewGitDAO())

	t.Run("root", func(t *testing.T) {
		ret, err := s.List("")
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies", "frameworks/solid"}, ret)
	})

	t.Run("component", func(t *testing.T) {
		ret, err := s.List("calligra")
		assert.NoError(t, err)
		assert.Equal(t, []string{"calligra/krita", "calligra/krita-extensions/krita-analogies"}, ret)
	})

	t.Run("project", func(t *testing.T) {
		ret, err := s.List("calligra/krita")
		assert.NoError(t, err)
		// This must not include krita-extensions as it is not a child, it happens
		// to have the same stringy prefix though.
		assert.Equal(t, []string{"calligra/krita"}, ret)
	})
}
