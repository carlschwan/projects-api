/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package services

import (
	"path/filepath"
	"strings"

	"anongit.kde.org/sysadmin/projects-api.git/models"
)

type ProjectService struct {
	dao gitDAO
}

func NewProjectService(dao gitDAO) *ProjectService {
	return &ProjectService{dao}
}

func (s *ProjectService) Get(path string) (*models.Project, error) {
	return s.dao.Get(path)
}

func (s *ProjectService) isProject(path string) bool {
	model, err := s.Get(path)
	if err != nil {
		panic(err)
	}
	return model != nil && model.Repo != ""
}

func (s *ProjectService) Find(filter *models.ProjectFilter) ([]string, error) {
	matches := []string{}

	for _, path := range s.dao.List() {
		if !s.isProject(path) {
			continue
		}

		if len(filter.ID) != 0 && filter.ID != filepath.Base(path) {
			continue
		}
		model, _ := s.Get(path)
		if len(filter.RepoPath) != 0 {
			if model.Repo != filter.RepoPath {
				continue // doesn't match repopath constraint
			}
		}
		if filter.ActiveOnly && !model.Active {
			continue
		}
		if filter.InactiveOnly && model.Active {
			continue
		}

		matches = append(matches, path)
	}

	return matches, nil
}

func (s *ProjectService) List(prefix string) ([]string, error) {
	matches := []string{}

	for _, path := range s.dao.List() {
		if !s.isProject(path) {
			continue
		}

		if len(prefix) == 0 || path == prefix || strings.HasPrefix(path, prefix+"/") {
			matches = append(matches, path)
		}
	}

	return matches, nil
}
