/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package daos

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Shared dao so tests don't clone over and over again (implies that
// TestGitUpdateClone is working ;))
var dao = newGitDAOInternal(false)

func TestGitUpdateClone(t *testing.T) {
	tmpdir, _ := ioutil.TempDir("", "")
	twd, _ := os.Getwd()
	twd = twd + "/"
	os.Chdir(tmpdir)
	defer func() {
		os.Chdir(twd)
		os.RemoveAll(tmpdir)
	}()
	// DO NOT MUTATE TWD BELOW!

	// No clone
	pwd, _ := os.Getwd()
	fmt.Println(pwd)
	_, err := os.Stat("repo-metadata")
	assert.Error(t, err)

	cloneDAO := newGitDAOInternal(false)
	cloneDAO.UpdateClone()

	// Clone now
	pwd, _ = os.Getwd()
	fmt.Println(pwd)
	_, err = os.Stat("repo-metadata")
	assert.NoError(t, err)
}

func TestGitGet(t *testing.T) {
	project, err := dao.Get("frameworks/solid")

	assert.NoError(t, err)
	assert.NotNil(t, project)
	assert.Equal(t, "frameworks/solid", project.Path)
	assert.Equal(t, "solid", project.Repo)
	assert.Equal(t, "master", project.I18n.TrunkKF5)
	assert.Equal(t, "none", project.I18n.StableKF5)
	assert.Equal(t, true, project.Active)
}

func TestGitGetComponent(t *testing.T) {
	project, err := dao.Get("frameworks")

	assert.NoError(t, err)
	assert.Nil(t, project)
}

func TestGitList(t *testing.T) {
	// We work on live data, so we can't assert much here. Other than list not
	// being empty.
	assert.NotEmpty(t, dao.List())
}

func TestMain(m *testing.M) {
	tmpdir, _ := ioutil.TempDir("", "")
	twd, _ := os.Getwd()
	twd = twd + "/"
	os.Chdir(tmpdir)
	defer func() {
		os.Chdir(twd)
		os.RemoveAll(tmpdir)
	}()
	// DO NOT MUTATE TWD BELOW!

	dao.UpdateClone()

	os.Exit(m.Run())
}
