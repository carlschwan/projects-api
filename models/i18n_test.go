/*
	Copyright © 2017 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package models

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInfer(t *testing.T) {
	// Testing component inference. Given the infer function a project should
	// correctly infer the i18n component from the available information.
	cases := []struct {
		path      string
		component string
	}{
		{"calligra/krita", "calligra"},
		{"kde/workspace/plasma-workspace", "kde-workspace"},
		{"kde/kdemultimedia/dragon", "kdemultimedia"},
		{"frameworks/solid", "frameworks"},
		{"extragear/utils/telepathy/fishy", "extragear-utils"},
		// Make sure i18n_path of modules that are also projects get properly
		// constructed.
		// https://bugs.kde.org/show_bug.cgi?id=379164
		{"kde/kdepim-runtime", "kdepim-runtime"},
		// Make sure i18n_path of modules that are inside projects on a third level
		// nested get properly constructed.
		// https://bugs.kde.org/show_bug.cgi?id=379161
		{"kde/kdegraphics/libs/libksane", "kdegraphics"},
		// Phonon does not follow the general rule, it's nested in the project
		// structure but flattened in i18n.
		{"kdesupport/phonon/phonon-vlc", "kdesupport"},
		// ... in case it gets flattened in project structure as well, let's
		// ensure it will behave correctly still.
		{"kdesupport/phonon", "kdesupport"},
	}
	for _, testCase := range cases {
		t.Run(testCase.path, func(t *testing.T) {
			i := I18nData{}
			p := Project{Path: testCase.path, Repo: filepath.Base(testCase.path)}
			i.Infer(&p)
			assert.Equal(t, testCase.component, i.Component)
		})
	}
}

func testInferIdentifier(t *testing.T) {
	// Not actually an inference. If the Project has an Identifier set, it's the
	// component since the gitlab transition.
	i := I18nData{}
	p := Project{
		Path:       "kdesupport/phonon",
		Repo:       "kdesupport/phonon",
		Identifier: "foobar",
	}
	i.Infer(&p)
	assert.Equal(t, "foobar", i.Component)
}
