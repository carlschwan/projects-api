/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package models

// Project is the core model of a project entity.
type Project struct {
	I18n       I18nData `yaml:"i18n" json:"i18n"`
	Path       string   `yaml:"projectpath" json:"path"`
	Repo       string   `yaml:"repopath" json:"repo"`
	Active     bool     `yaml:"repoactive" json:"-"` // do not marshal to json, we presently have no use case for it
	Identifier string   `yaml:"identifier" json:"-"` // marshal'd via I18nData
}
