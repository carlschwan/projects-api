/*
	Copyright © 2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package models

// ProjectFilter is used to filter projects on find() calls.
// The type(names) in this struct must be chosen so that an uninitialized
// filter results in match-all behavior!
type ProjectFilter struct {
	ID           string `form:"id"`       // basename of project path
	RepoPath     string `form:"repo"`     // complete repo path
	ActiveOnly   bool   `form:"active"`   // whether to select active projects
	InactiveOnly bool   `form:"inactive"` // whether to select inactive projects
}
