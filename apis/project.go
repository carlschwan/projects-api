/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package apis

import (
	"net/http"
	"strings"

	"anongit.kde.org/sysadmin/projects-api.git/models"

	"github.com/gin-gonic/gin"
)

type projectService interface {
	Get(path string) (*models.Project, error)
	Find(filter *models.ProjectFilter) ([]string, error)
	List(path string) ([]string, error)
}

type projectResource struct {
	service projectService
}

// ServeProjectResource creates a new API resource.
func ServeProjectResource(rg *gin.RouterGroup, service projectService) {
	r := &projectResource{service}
	rg.GET("/repo/*path", r.repo)
	rg.GET("/project/*path", r.get)
	rg.GET("/find", r.find)
	rg.GET("/projects", r.projects)
	rg.GET("/projects/*path", r.projects)
}

/**
 * @apiDefine ProjectSuccessExample
 * @apiSuccessExample {json} Success-Response:
 *   {
 *     "i18n": {
 *       "stable": "none",
 *       "stableKF5": "krita/3.1",
 *       "trunk": "none",
 *       "trunkKF5": "master",
 *       "component": "calligra"
 *     },
 *     "repo": "krita"
 *     "path": "calligra/krita"
 *   }
 */

/**
 * @api {get} /project/:path Get (by Project)
 *
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiName project
 *
 * @apiDescription Gets the metadata of the project identified by <code>path</code>.
 *
 * @apiUse ProjectSuccessExample
 *
 * @apiError Forbidden Path may not be accessed.
 */
func (r *projectResource) get(c *gin.Context) {
	path := strings.TrimLeft(c.Param("path"), "/")

	if strings.Contains(path, "/..") || strings.Contains(path, "../") {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	response, err := r.service.Get(path)
	if err != nil {
		panic(err)
	}

	if response == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, response)
}

/**
 * @api {get} /repo/:path Get (by Repository)
 * @apiParam {String} path Repository path as seen in <code>git clone kde:path/is/this</code>.
 *
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiName repo
 *
 * @apiDescription Gets the metadata of the project identified by a query.
 *
 * @apiUse ProjectSuccessExample
 *
 * @apiError NotFound Couldn't find a project associated with this repo.
 * @apiError MultipleChoices Couldn't find a unique project for this repo.
 *   You'll need to pick a project and get it via the <code>/project/</code>
 *   endpoint
 * @apiErrorExample {json} MultipleChoices-Response:
 *   [
 *   "books/kf5book",
 *   "books/kf5book-duplcate"
 *   ]
 */
func (r *projectResource) repo(c *gin.Context) {
	path := strings.TrimLeft(c.Param("path"), "/")

	matches, err := r.service.Find(&models.ProjectFilter{RepoPath: path})
	if err != nil {
		panic(err)
	}

	if len(matches) < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if len(matches) > 1 {
		c.JSON(http.StatusMultipleChoices, matches)
		return
	}

	response, err := r.service.Get(matches[0])
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, response)
}

/**
 * @api {get} /find Find
 * @apiParam {String} id Identifier (basename) of the project to find.
 * @apiParam {String} repo <code>repo</code> attribute of the project
 *   to find.
 * @apiParam {Bool} active Whether to find only projects marked active (inactive
 *    projects can be skipped by code that wants to iterate worthwhile code
 *    projects only e.g. sysadmin repos are usually inactive).
 *    This defaults to false, giving you all repos.
 * @apiParam {Bool} inactive Whether to find only inactive projects (see active)
 *    This defaults to false, giving you all repos.
 *
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiName find
 *
 * @apiDescription Finds matching projects by a combination of filter params or
 *   none to list all projects.
 *
 * @apiError NotFound Nothing matched the filter criteria
 *
 * @apiSuccessExample {json} Success-Response:
 *   [
 *   "books",
 *   "books/kf5book",
 *   "calligra",
 *   ...
 *   ]
 */
func (r *projectResource) find(c *gin.Context) {
	var filter models.ProjectFilter
	c.BindQuery(&filter)
	matches, err := r.service.Find(&filter)
	if len(matches) == 0 || err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	c.JSON(http.StatusOK, matches)
}

/**
 * @api {get} /projects/:prefix List
 * @apiParam {String} prefix Prefix path of the project. This will usually be
 *   the module/components the project is sorted under.
 *
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiName projects
 *
 * @apiDescription Lists all *projects* underneath the prefix. If the prefix is
 *   a project itself it will be included in the list. If the prefix is not a
 *   project only its "children" will be returned.
 *
 * @apiSuccessExample {json} Project (/projects/calligra/krita):
 *   [
 *     "calligra/krita"
 *   ]
 *
 * @apiSuccessExample {json} Project with Children (/projects/papa):
 *   [
 *     "aba",
 *     "aba/bubbale1",
 *     "aba/bubbale2"
 *   ]
 *
 * @apiSuccessExample {json} Component (/projects/frameworks)
 *   [
 *     "frameworks/solid",
 *     "frameworks/ki18n",
 *     "..."
 *   ]
 */
func (r *projectResource) projects(c *gin.Context) {
	path := strings.TrimLeft(c.Param("path"), "/")

	matches, err := r.service.List(path)
	if len(matches) == 0 || err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, matches)
}
