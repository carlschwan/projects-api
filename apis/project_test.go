/*
	Copyright © 2017-2019 Harald Sitter <sitter@kde.org>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of
	the License or any later version accepted by the membership of
	KDE e.V. (or its successor approved by the membership of KDE
	e.V.), which shall act as a proxy defined in Section 14 of
	version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package apis

import (
	"errors"
	"net/http"
	"testing"

	"anongit.kde.org/sysadmin/projects-api.git/models"
)

// Test Double
type ProjectService struct {
}

func NewProjectService() *ProjectService {
	return &ProjectService{}
}

func (s *ProjectService) Get(path string) (*models.Project, error) {
	project := models.Project{}
	if path == "calligra/krita" {
		project.Path = "calligra/krita"
		project.Repo = "krita"
		return &project, nil
	}
	if path == "calligra" {
		return nil, nil
	}
	return &project, errors.New("unexpected path " + path)
}

func (s *ProjectService) Find(filter *models.ProjectFilter) ([]string, error) {
	projects := []string{"calligra/krita"}
	if filter.ID == "krita" && filter.RepoPath == "" {
		return projects, nil
	}
	if filter.ID == "" && filter.RepoPath == "krita" {
		return projects, nil
	}
	if filter.ID == "" && filter.InactiveOnly {
		return []string{"kde/hole"}, nil
	}
	if filter.ID == "" && filter.RepoPath == "" {
		return append(projects, "frameworks/solid"), nil
	}
	panic("unexpected query " + filter.ID + " " + filter.RepoPath)
}

func (s *ProjectService) List(path string) ([]string, error) {
	if path == "" {
		return []string{"calligra/krita", "frameworks/solid"}, nil
	}
	if path == "calligra" {
		return []string{"calligra/krita"}, nil
	}
	panic("unexpected query " + path)
}

func init() {
	v1 := router.Group("/v1")
	{
		ServeProjectResource(v1, NewProjectService())
	}
}

func TestProject(t *testing.T) {
	kritaObj := `{"i18n":{"trunkKF5":"", "component":"", "stable":"", "stableKF5":"", "trunk":""}, "repo":"krita", "path":"calligra/krita"}`
	runAPITests(t, []apiTestCase{
		{"get a project", "GET", "/v1/project/calligra/krita", "", http.StatusOK, kritaObj},
		{"get nil project", "GET", "/v1/project/calligra", "", http.StatusNotFound, ""},
		{"get by repopath", "GET", "/v1/repo/krita", "", http.StatusOK, kritaObj},
		{"find by id", "GET", "/v1/find?id=krita", "", http.StatusOK, `["calligra/krita"]`},
		{"find by repopath", "GET", "/v1/find?repo=krita", "", http.StatusOK, `["calligra/krita"]`},
		{"find by only inactive", "GET", "/v1/find?inactive=true", "", http.StatusOK, `["kde/hole"]`},
		{"find all", "GET", "/v1/find", "", http.StatusOK, `["calligra/krita", "frameworks/solid"]`},
		{"list", "GET", "/v1/projects", "", http.StatusOK, `["calligra/krita", "frameworks/solid"]`},
		{"list component", "GET", "/v1/projects/calligra", "", http.StatusOK, `["calligra/krita"]`},
	})
}
